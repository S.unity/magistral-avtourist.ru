<?php
    // Allow from any origin
     if(isset($_SERVER["HTTP_ORIGIN"]))
     {
        // You can decide if the origin in $_SERVER['HTTP_ORIGIN'] is something you want to
      // allow, or as we do here, just allow all
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
     }
     else
     {
        //No HTTP_ORIGIN set, so we allow any. You can disallow if needed here
        header("Access-Control-Allow-Origin: *");
     }

     header("Access-Control-Allow-Credentials: true");
     header("Access-Control-Max-Age: 600");    // cache for 10 minutes

     if($_SERVER["REQUEST_METHOD"] == "OPTIONS")
     {
        if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"]))
            header("Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PUT");
  //Make
        // sure you remove those you do not want to support

        if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]))
            header("Access-Control-Allow-Headers:
           {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

           //Just exit with 200 OK with the above headers for OPTIONS method
        exit(0);
      }
    //From here, handle the request as it is ok


     $name = !empty($_POST['name']) ? $_POST['name'] : '';
     $email = !empty($_POST['email']) ? $_POST['email'] : '';
     $phone = !empty($_POST['phone']) ? $_POST['phone'] : '';
     $subject = !empty($_POST['subject']) ? $_POST['subject'] : '';
     $message = !empty($_POST['message']) ? $_POST['message'] : '';
     $note = !empty($_POST['note']) ? $_POST['note'] : '';

     $message = "$message";

     $to_email = 'avtourist-msk-rus@yandex.ru';
     $subject = "Заявка: <br> <span style='font-weight: bold'>$message</span> <br><br> $note <br><br> Имя: <span style='font-weight: bold'>$name</span> <br><br> Телефон: <span style='font-weight: bold'>$phone</span> <br><br> Почта: <span style='font-weight: bold'>$email</span>";
     $headers[] = 'MIME-Version: 1.0';
     $headers[] = 'Content-type: text/html; charset=UTF-8';
     // $headers[] = "From: <$email>";

     mail($to_email, "Заявка с magistral-avtourist.ru", $subject, implode("\r\n", $headers));

    ?>
