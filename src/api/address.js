import axios from 'axios'
export default () => {
  return axios.create({
    baseURL: 'https://crm1-api.technik-crm.ru'
  })
}
